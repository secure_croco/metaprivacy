# About Metaprivacy

**Metaprivacy** is a humble page dedicated to the aggregation of privacy resources, guides, tools, news, and opinions.

> “Arguing that you don't care about the right to privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say.”

― [Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden)

> “Google and Facebook's insidious control of our digital lives undermines the very essence of privacy and is one of the defining human rights challenges of our era.”

― [Kumi Naidoo](https://en.wikipedia.org/wiki/Kumi_Naidoo)

 > “The choice for mankind lies between freedom and happiness and for the great bulk of mankind, happiness is better.”

― [George Orwell](https://en.wikipedia.org/wiki/George_Orwell)

# Contents

[[_TOC_]]

# Section 1) **Privacy Resources & Guides**

## Technical Privacy Resources

- 🌟 [Privacy Guides](https://privacyguides.org/) — [Source code](https://github.com/privacyguides/privacyguides.org)
- [Surveillance Self-Defense](https://ssd.eff.org/)
- [Techlore](https://techlore.tech/)
- [The New Oil](https://thenewoil.org/)
- [PRISM Break](https://prism-break.org/en/) — [Source code](https://gitlab.com/prism-break/prism-break)
- [switching.software](https://switching.software/)
- [Opensource.Builders](https://opensource.builders/)
- [Awesome Privacy](https://pluja.github.io/awesome-privacy/) (by [Pluja](https://github.com/pluja)) — [Source code](https://github.com/pluja/awesome-privacy)
- [Awesome Privacy](https://awesome-privacy.xyz/#/) (by [Alicia](https://github.com/Lissy93)) — [Source code](https://github.com/Lissy93/awesome-privacy)
- [Delightful Humane Design](https://codeberg.org/teaserbot-labs/delightful-humane-design)
- [AlternativeTo](https://alternativeto.net/)
- [gofoss.net](https://gofoss.net/)
- [ThinkPrivacy](https://thinkprivacy.net/)
- [PrivacyTests.org](https://privacytests.org/)
- [Secure Messaging Apps Comparison](https://www.securemessagingapps.com/)

## Ethical / Humane Technology Foundations

- [Center for Humane Technology](https://www.humanetech.com/)
- [Software Freedom Conservancy](https://sfconservancy.org/)

## Privacy Communities

- [Humane Tech Community](https://humanetech.community/) — [Forum](https://community.humanetech.com/)
- [r/PrivacyGuides on Reddit](https://www.reddit.com/r/PrivacyGuides/)
- [r/privacy on Reddit](https://www.reddit.com/r/privacy/)
- [r/TOR on Reddit](https://www.reddit.com/r/TOR/)

## Legal Privacy Resources

- [Terms of Service; Didn't Read](https://tosdr.org/)
- [My Privacy is None of Your Business (NYOB)](https://noyb.eu/en)

## Privacy Blogs

- [Privacy Guides Blog](https://blog.privacyguides.org/)
- [DuckDuckGo Blog](https://spreadprivacy.com/)
- [Proton Blog](https://proton.me/blog)
- [Proton VPN Blog](https://protonvpn.com/blog/)
- [Mozilla Blog](https://blog.mozilla.org/)
- [Signal Blog](https://signal.org/blog/)

## Educational Videos

- [(2022-04) Data Brokers: Last Week Tonight with John Oliver (HBO) (YouTube)](https://www.youtube.com/watch?v=wqn3gR1WTcA)
- [(2021-03) How our personal data is exploited in unexpected ways (BBC)](https://www.bbc.com/reel/video/p09blhfw/how-our-personal-data-is-exploited-in-unexpected-ways)
- [(2020-08) Become Anonymous: The Ultimate Guide To Privacy, Security, & Anonymity 2020 (YouTube)](https://www.youtube.com/watch?v=a1i-3xwcSGA)
- [(2019-10) Joe Rogan Experience #1368 - Edward Snowden (YouTube)](https://www.youtube.com/watch?v=efs3QRr8LWw)
- [(2019-04) Microsoft Should be VERY Afraid - Noob's Guide to Linux Gaming (YouTube)](https://www.youtube.com/watch?v=Co6FePZoNgE)
- [(2018-05) Why Bing Isn't a Failure (& the Future of the Internet) (YouTube)](https://www.youtube.com/watch?v=mviTS_cIWXg)
- 🌟 [(2018-04) Jaron Lanier: How we need to remake the internet (TED)](https://www.ted.com/talks/jaron_lanier_how_we_need_to_remake_the_internet)
- [(2017-04) Tristan Harris: How a handful of tech companies control billions of minds every day (TED)](https://www.ted.com/talks/tristan_harris_how_a_handful_of_tech_companies_control_billions_of_minds_every_day)
- [(2016-12) The Terrifying Cost of "Free” Websites | Adam Ruins Everything (YouTube)](https://www.youtube.com/watch?v=5pFX2P7JLwA)
- [(2014-10) Glenn Greenwald: Why privacy matters (TED)](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters)

## Social Media

- [Why every social media site is a dumpster fire (YouTube)](https://www.youtube.com/watch?v=wZSRxfHMr5s)
- [Quit social media | Dr. Cal Newport | TEDxTysons (YouTube)](https://www.youtube.com/watch?v=3E7hkPZ-HTk)
- [Jaron Lanier interview on how social media ruins your life (YouTube)](https://www.youtube.com/watch?v=kc_Jq42Og7Q)
- [Why Instagram Is Ruining Your Life (YouTube)](https://www.youtube.com/watch?v=NGeU17Df8cw)

## How to Live Without Google

- [What does Google know about me? (Quora)](https://www.quora.com/What-does-Google-know-about-me/answer/Gabriel-Weinberg)
- [How to Live Without Google: Alternatives That Protect Your Privacy (DuckDuckGo Blog)](https://spreadprivacy.com/how-to-remove-google/)
- [How to live without Google (AlternativeTo)](https://alternativeto.net/list/818/how-to-live-without-google)
- [The Complete List of Alternatives to all Google Products (Waking Times)](https://www.wakingtimes.com/2019/07/19/the-complete-list-of-alternatives-to-all-google-products/)

## Windows Privacy

- [Comparison of Windows 10 Privacy tools (Ghacks)](https://www.ghacks.net/2015/08/14/comparison-of-windows-10-privacy-tools/)

## Printer Privacy

- [Yellow Dots of Mystery: Is Your Printer Spying on You? (YouTube)](https://www.youtube.com/watch?v=izMGMsIZK4U)
- [Printer Tracking (EFF)](https://www.eff.org/issues/printers)
- [Printer Dots (EFF)](https://www.eff.org/cases/foia-printer-dots)
- [List of Printers Which Do or Do Not Display Tracking Dots (EFF)](https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots)
- [Machine Identification Code (Wikipedia)](https://en.wikipedia.org/wiki/Machine_Identification_Code)

# Section 2) **Privacy-Focused Software**

## Service Providers

| **Purpose**        | **Service Provider**                                                                                                                             |
|:-------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
| *Search*           | [DuckDuckGo](https://duckduckgo.com/), [Kagi](https://kagi.com/), [SearXNG](https://docs.searxng.org/)                                           |
| *Email*            | [Proton Mail](https://proton.me/mail), [Tutanota](https://tutanota.com/)                                                                         |
| *VPN*              | [Proton VPN](https://protonvpn.com/), [Mozilla VPN](https://www.mozilla.org/en-US/products/vpn/) (powered by [Mullvad](https://mullvad.net/en/)) |
| *Social media*     | [Mastodon](https://joinmastodon.org/)                                                                                                            |
| *Internet forum*   | [Tildes](https://tildes.net/), [Lemmy](https://join-lemmy.org/)                                                                                  |
| *Video sharing*    | [Invidious](https://invidious.io/), [Odysee](https://odysee.com/) (based on [LBRY](https://lbry.com/)), [PeerTube](https://joinpeertube.org/)    |

## Applications

| **Purpose**        | **Application**                                                                                   |
|:-------------------|:--------------------------------------------------------------------------------------------------|
| *Browser*          | [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Tor Browser](https://www.torproject.org/) |
| *Messaging*        | [Signal](https://www.signal.org/), [Element](https://element.io/)                                 |
| *Password manager* | [Bitwarden](https://bitwarden.com/)                                                               |
| *Note taking*      | [Standard Notes](https://standardnotes.org/), [Joplin](https://joplinapp.org/)                    |
| *Office suite*     | [LibreOffice](https://www.libreoffice.org/)                                                       |
| *Code editor*      | [VSCodium](https://vscodium.com/)                                                                 |
| *Web analytics*    | [Matomo](https://matomo.org/)                                                                     |

## Operating Systems (OS)

| **Purpose**        | **Operating System (OS)**                                                                                                                                 |
|:-------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------|
| *Mobile OS*        | [iOS](https://en.wikipedia.org/wiki/IOS), [GrapheneOS](https://grapheneos.org/)                                                                           |
| *Desktop OS*       | [macOS](https://en.wikipedia.org/wiki/MacOS), [Manjaro](https://manjaro.org/), [Pop!_OS](https://pop.system76.com/), [Linux Mint](https://linuxmint.com/) |
| *Live USB*         | [Tails](https://tails.boum.org/)                                                                                                                          |
| *Router firmware*  | [OpenWrt](https://openwrt.org/)                                                                                                                           |

## Firefox Extensions to Increase Privacy

### Recommended

- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)

### Optional

- [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)

# Section 3) **Privacy-Focused Hardware**

## Guide to Shop for Privacy-Respecting Devices

- [*Privacy Not Included](https://foundation.mozilla.org/en/privacynotincluded/)

## Privacy-Focused Hardware Makers

- [System76](https://system76.com/)
- [Purism](https://puri.sm/)
- [Pine64](https://www.pine64.org/)

# Section 4.1) **Privacy Films**

- [The Social Dilemma](https://en.wikipedia.org/wiki/The_Social_Dilemma)
- [Citizenfour](https://en.wikipedia.org/wiki/Citizenfour)

# Section 4.2) **Privacy Books**

- [Recommended Reading on r/privacy](https://www.reddit.com/r/privacy/wiki/recommended_reading)
- [Permanent Record by Edward Snowden](https://en.wikipedia.org/wiki/Permanent_Record_(autobiography))

# Section 4.3) **Wikipedia Articles**

- [Global surveillance](https://en.wikipedia.org/wiki/Global_surveillance)
- [Surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism)
- [Differential privacy](https://en.wikipedia.org/wiki/Differential_privacy)
- [End-to-end encryption](https://en.wikipedia.org/wiki/End-to-end_encryption)
- [Reproducible builds](https://en.wikipedia.org/wiki/Reproducible_builds)
- [Warrant canary](https://en.wikipedia.org/wiki/Warrant_canary)

# Section 5) **Privacy News & Opinions**

## (Various)

- [(2019-11) Tim Berners-Lee unveils global plan to save the web (The Guardian)](https://www.theguardian.com/technology/2019/nov/24/tim-berners-lee-unveils-global-plan-to-save-the-internet)
- [(2019-11) Facebook and Google’s pervasive surveillance poses an unprecedented danger to human rights (Amnesty International)](https://www.amnesty.org/en/latest/news/2019/11/google-facebook-surveillance-privacy/)
- [(2018-11) Surveillance Kills Freedom By Killing Experimentation (Wired)](https://www.wired.com/story/mcsweeneys-excerpt-the-right-to-experiment/)
- [(2018-04) Facebook and Apple Embody New Tech Divide (Barron's)](https://www.barrons.com/articles/facebook-and-apple-embody-new-tech-divide-1524273880)

## Google

- [(2020-10) Google Meddling With URLs In Emails, Causing Security Concerns (Hackaday)](https://hackaday.com/2020/10/21/google-meddling-with-urls-in-emails-causing-security-concerns/)
- [(2020-08) Google accidentally enables Home smart speakers to listen in to everyday house sounds (The Independent)](https://www.independent.co.uk/life-style/gadgets-and-tech/news/google-home-smart-speakers-listen-switch-on-smoke-detector-glass-breaking-a9652991.html)
- [(2019-06) Google’s new reCAPTCHA has a dark side (Fast Company)](https://www.fastcompany.com/90369697/googles-new-recaptcha-has-a-dark-side)
- [(2019-06) Goodbye, Chrome: Google’s Web browser has become spy software (The Washington Post)](https://www.washingtonpost.com/technology/2019/06/21/google-chrome-has-become-surveillance-software-its-time-switch/)
- [(2019-01) France fines Google nearly $57 million for first major violation of new European privacy regime (The Washington Post)](https://www.washingtonpost.com/world/europe/france-fines-google-nearly-57-million-for-first-major-violation-of-new-european-privacy-regime/2019/01/21/89e7ee08-1d8f-11e9-a759-2b8541bbbe20_story.html)
- [(2018-09) Google Suppresses Memo Revealing Plans to Closely Track Search Users in China (The Intercept)](https://theintercept.com/2018/09/21/google-suppresses-memo-revealing-plans-to-closely-track-search-users-in-china/)

## Facebook

- [(2021-11) This thought experiment captures Facebook’s betrayal of users’ privacy (The Guardian)](https://www.theguardian.com/commentisfree/2021/nov/03/thought-experiment-facebook-betrayal-privacy)
- [(2021-05) The Instagram ads Facebook won't show you (Signal Blog)](https://signal.org/blog/the-instagram-ads-you-will-never-see/)
- [(2021-03) Instagram is ‘most invasive app’, new study shows (The Independent)](https://www.independent.co.uk/life-style/gadgets-and-tech/instagram-invasive-app-privacy-facebook-b1818453.html)
- [(2019-11) Facebook admits to circumventing GDPR (Enterprise Times)](https://www.enterprisetimes.co.uk/2019/11/12/facebook-admits-to-circumventing-gdpr/)
- [(2019-01) Apple says it’s banning Facebook’s research app that collects users’ personal information (Vox)](https://www.vox.com/2019/1/30/18203231/apple-banning-facebook-research-app)

## Apple

- [(2020-07) Hong Kong’s protest movement keeps getting stymied by Apple (Quartz)](https://qz.com/1879754/hong-kongs-protest-movement-stymied-by-apples-china-ties/)
- [(2020-06) How Apple uses anti-competitive practices to extort developers and support authoritarian regimes (ProtonMail Blog)](https://protonmail.com/blog/apple-app-store-antitrust/)
- [(2020-01) Apple dropped plan for encrypting backups after FBI complained (Reuters)](https://www.reuters.com/article/us-apple-fbi-icloud-exclusive/exclusive-apple-dropped-plan-for-encrypting-backups-after-fbi-complained-sources-idUSKBN1ZK1CT)
- [(2019-11) ‘The time is now to have a federal privacy bill,’ says Tim Cook (The Verge)](https://www.theverge.com/2019/11/22/20978140/tim-cook-apple-federal-privacy-bill-facebook-breakup-big-tech)
- [(2018-12) Apple admits giving governments access to thousands of iPhones and other devices (The Independent)](https://www.independent.co.uk/life-style/gadgets-and-tech/news/apple-iphone-ipad-government-data-privacy-transparency-report-2018-a8697761.html)
- [(2018-09) Apple's Pious Privacy Pledges Ring Hollow (TheStreet)](https://www.thestreet.com/investing/stocks/apple-s-pious-privacy-pledges-ring-hollow-14696402)
- [(2016-06) Apple’s ‘Differential Privacy’ Is About Collecting Your Data---But Not ​Your Data (Wired)](https://www.wired.com/2016/06/apples-differential-privacy-collecting-data/)

## Microsoft

- [(2020-12) Microsoft apologises for feature criticised as workplace surveillance (The Guardian)](https://www.theguardian.com/technology/2020/dec/02/microsoft-apologises-productivity-score-critics-derided-workplace-surveillance)
- [(2016-02) Windows 10 Sends Your Data 5500 Times Every Day Even After Tweaking Privacy Settings (The Hacker News)](https://thehackernews.com/2016/02/microsoft-windows10-privacy.html)
- [(2015-08) Windows 10 Reserves The Right To Block Pirated Games And 'Unauthorized' Hardware (Techdirt)](https://www.techdirt.com/articles/20150820/06171332012/windows-10-reserves-right-to-block-pirated-games-unauthorized-hardware.shtml)
- [(2015-08) Even when told not to, Windows 10 just can’t stop talking to Microsoft (Ars Technica)](https://arstechnica.com/information-technology/2015/08/even-when-told-not-to-windows-10-just-cant-stop-talking-to-microsoft/)

## Amazon

- [(2021-03) Civil Rights Groups Want Tech Sites to Stop Reviewing Amazon's Ring Cameras (Vice)](https://www.vice.com/en/article/z3vpw3/civil-rights-groups-want-tech-sites-to-stop-reviewing-amazons-ring-cameras)

## Spotify

- [(2021-05) Over 180 Musicians Protest Spotify’s Speech Monitoring Patent in Open Letter (Pitchfork)](https://pitchfork.com/news/over-180-musicians-protest-spotify-speech-monitoring-patent-in-open-letter/)

## Twitter

- [(2020-08) Twitter About To Be Hit With A ~$250 Million Fine For Using Your Two Factor Authentication Phone Numbers/Emails For Marketing (Techdirt)](https://www.techdirt.com/articles/20200804/01231345032/twitter-about-to-be-hit-with-250-million-fine-using-your-two-factor-authentication-phone-numbers-emails-marketing.shtml)

## Yahoo

- [(2016-10) What Yahoo’s NSA Surveillance Means for Email Privacy (ProtonMail Blog)](https://protonmail.com/blog/yahoo-us-intelligence/)

## Tesla

- [(2021-03) Tesla's in-car cameras raise privacy concerns: Consumer Reports (Reuters)](https://www.reuters.com/article/us-tesla-privacy/teslas-in-car-cameras-raise-privacy-concerns-consumer-reports-idUSKBN2BF2MM)

## Firefox

- [(2019-11) Firefox’s fight for the future of the web (The Guardian)](https://www.theguardian.com/technology/2019/nov/17/firefox-mozilla-fights-back-against-google-chrome-dominance-privacy-fears)
- [(2019-01) Firefox makes it easier for users to dodge ad-trackers (Naked Security)](https://nakedsecurity.sophos.com/2019/01/30/firefox-makes-ad-tracker-dodging-easier-for-privacy-conscious-users/)

## Signal

- [(2016-10) Signal, the Cypherpunk App of Choice, Adds Disappearing Messages (Wired)](https://www.wired.com/2016/10/signal-cypherpunk-app-choice-adds-disappearing-messages/)

## Topic — Smart TVs

- [(2020-01) Given smart TV privacy risks, you might be better off with a ‘dumb’ TV (ProtonVPN Blog)](https://protonvpn.com/blog/smart-tv-privacy-risk/)
- [(2019-12) Now even the FBI is warning about your smart TV’s security (TechCrunch)](https://techcrunch.com/2019/12/01/fbi-smart-tv-security/)

## Topic — Fitness Apps

- [(2019-10) Fitness apps are good for your health, but often bad for your privacy (ProtonVPN Blog)](https://protonvpn.com/blog/fitness-apps-are-good-for-your-health-but-often-bad-for-your-privacy/)
- [(2016-05) Runkeeper background tracking leads to complaint from privacy watchdog (Ars Technica)](https://arstechnica.com/tech-policy/2016/05/runkeeper-fitnesskeeper-breaches-data-protection-law-norway/)

## Topic — Networking

- [(2020-03) OpenWRT code-execution bug puts millions of devices at risk (Ars Technica)](https://arstechnica.com/information-technology/2020/03/openwrt-is-vulnerable-to-attacks-that-execute-malicious-code/)
- [(2019-08) Huge Survey of Firmware Finds No Security Gains in 15 Years (The Security Ledger)](https://securityledger.com/2019/08/huge-survey-of-firmware-finds-no-security-gains-in-15-years/)
- [(2018-06) Millions of Streaming Devices Are Vulnerable to a Retro Web Attack (Wired)](https://www.wired.com/story/chromecast-roku-sonos-dns-rebinding-vulnerability/)

# Section 6) **Advanced Security / Technical Topics**

## Router Security

- https://routersecurity.org/#StartHere

## Browser Fingerprinting

- https://en.wikipedia.org/wiki/Device_fingerprint
- https://coveryourtracks.eff.org/
- https://amiunique.org/fp
- https://pixelprivacy.com/resources/browser-fingerprinting/

## Search Engine for Internet-Connected Devices

- https://www.shodan.io/

## IP Leak Detection

- https://ipleak.net/

## Intel Management Engine (ME)

- https://en.wikipedia.org/wiki/Intel_Management_Engine
- https://www.youtube.com/watch?v=MujjuTWpQJk
- https://www.youtube.com/watch?v=2wTpkgBiySE

## IDN Homograph Attack

- https://en.wikipedia.org/wiki/IDN_homograph_attack

## DNS Rebinding

- https://en.wikipedia.org/wiki/DNS_rebinding

---

# License

Please see the [LICENSE](LICENSE) file.

# Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
